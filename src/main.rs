mod routes;
mod state;
mod entities;

use actix_web::{get, App, HttpResponse, HttpServer, Responder, web};
use sea_orm::{DatabaseConnection, Database, DbErr};

use crate::state::{PlopState, PlopConfig};

#[get("/")]
async fn index() -> impl Responder {
    HttpResponse::Ok().body("Trolling!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("Connecting to database...");
    let db: Result<DatabaseConnection, DbErr> = Database::connect("sqlite://sqlite.db").await;
    if let Err(err) = db {
        eprintln!("{err}");
        std::process::exit(1)
    } else {
        println!("Successfully connected to database!")
    }
    let state = PlopState {
        db: db.unwrap(),
        config: PlopConfig {
            server_name: "aptest.tyman.systems".to_string()
        }
    };

    HttpServer::new(move || {
        App::new()
            .app_data(
                web::Data::new(
                    state.clone()
                )
            )
            .service(index)
            .service(routes::well_known::webfinger::handle)
    })
    .bind(("0.0.0.0", 5638))?
    .run()
    .await
}