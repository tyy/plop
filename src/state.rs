use sea_orm::DatabaseConnection;

#[derive(Clone)]
pub struct PlopState {
    pub db: DatabaseConnection,
    pub config: PlopConfig
}

#[derive(Clone)]
pub struct PlopConfig {
    pub server_name: String
}