use actix_web::{web, HttpResponse, HttpResponseBuilder, http::StatusCode};
use sea_orm::EntityTrait;
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value as JsonValue};
use url::Url;

use crate::{entities::{prelude::*}, state::PlopState};

#[derive(Deserialize, Debug)]
pub struct WebFingerQuery {
    resource: String,
    // TODO Figure out a good way to get serde to deserialize duplicate rel keys into a vector, so this can be supported as per spec
    // rel: Vec<String>
}

#[derive(Serialize)]
struct JRDLink {
    rel: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    r#type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    href: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    titles: Option<Map<String, JsonValue>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    properties: Option<Map<String, JsonValue>>
}

#[derive(Serialize)]
struct JsonResourceDescriptor {
    subject: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    aliases: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    properties: Option<Map<String, JsonValue>>,
    links: Vec<JRDLink>
}

#[actix_web::get("/.well-known/webfinger")]
pub async fn handle(query: web::Query<WebFingerQuery>, state: web::Data<PlopState>) -> HttpResponse {
    let parsed_url = Url::parse(&query.resource);
    match parsed_url {
        Ok(url) => {
            match url.scheme() {
                "acct" => {
                    // Remove any preceeding "@" char (to turn @ty@example.com into ty@example.com)
                    let acct = url.path().trim_start_matches("@");
                    let [username, domain] = acct.split("@").collect::<Vec<&str>>()[..] else {
                        return HttpResponseBuilder::new(StatusCode::BAD_REQUEST).body("Invalid resource (acct URL contains invalid AP mention)")
                    };
                    let server_name = &state.config.server_name;
                    if domain != server_name {
                        return HttpResponseBuilder::new(StatusCode::BAD_REQUEST).body("Invalid resource (account is on a different server)")
                    }

                    // Check if username is in db
                    let database_entry = User::find_by_id(username).one(&state.db).await;
                    match database_entry {
                        Ok(model) => {
                            if let Some(user_model) = model {
                                let mut titles = Map::new();
                                titles.insert("und".to_string(), user_model.display_name.unwrap_or(user_model.username).into());

                                return HttpResponseBuilder::new(StatusCode::OK).content_type("application/jrd+json").json(
                                    JsonResourceDescriptor {
                                        subject: format!("acct:{username}@{server_name}"),
                                        aliases: None,
                                        properties: None,
                                        links: vec![
                                            JRDLink {
                                                rel: "self".to_string(),
                                                titles: Some(titles),
                                                r#type: Some("application/activity+json".to_string()),
                                                href: Some("TODO".to_string()), // TODO Url api handling or whatever
                                                properties: None
                                            }
                                        ]
                                    }
                                )
                            } else {
                                return HttpResponseBuilder::new(StatusCode::NOT_FOUND).body("Invalid resource (account not found)")
                            }
                        },
                        Err(e) => {
                            eprint!("{e}");
                            return HttpResponseBuilder::new(StatusCode::INTERNAL_SERVER_ERROR).body("Internal server error")
                        }
                    }
                },
                "https" | "http" => {
                    return HttpResponseBuilder::new(StatusCode::BAD_REQUEST).body("Invalid resource (TODO)")
                },
                _ => return HttpResponseBuilder::new(StatusCode::BAD_REQUEST).body("Invalid resource (not a valid URL scheme)")
            }
        },
        Err(_) => return HttpResponseBuilder::new(StatusCode::BAD_REQUEST).body("Invalid resource (not a URL)")
    }
}