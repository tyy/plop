use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(User::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(User::Username)
                            .string()
                            .primary_key()
                            .not_null()
                    )
                    .col(
                        ColumnDef::new(User::DisplayName)
                            .string()
                            .null()
                            .default("null")
                    )
                    .col(
                        ColumnDef::new(User::Bio)
                            .text()
                            .null()
                            .default("null")
                    )
                    .col(
                        ColumnDef::new(User::Properties)
                            .json()
                            .default("[]")
                    )
                    .col(
                        ColumnDef::new(User::Bot)
                            .boolean()
                            .default(false)
                            .not_null()
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(User::Table).to_owned())
            .await
    }
}

/// Learn more at https://docs.rs/sea-query#iden
#[derive(Iden)]
enum User {
    Table,
    // Username (such as "ty" in @ty@example.com), also the primary key to prevent duplicates
    Username,
    // Display name (such as "Ty")
    DisplayName,
    // User bio
    Bio,
    // Bot
    Bot,
    // Self-set properties
    Properties
}
